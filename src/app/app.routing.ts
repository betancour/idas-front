﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_helpers';
import { MigrantComponent } from './migrants';
import { MigrantGuard } from './_helpers/migrants.guard';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' },
  {
    path: 'migrants',
    loadChildren: 'app/migrants/migrants.module#Migrants',
    canActivate: [AuthGuard],
    canActivateChild: [MigrantGuard],
  },
];

export const appRoutingModule = RouterModule.forRoot(routes);
