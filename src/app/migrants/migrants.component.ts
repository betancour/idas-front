import { Component, OnInit } from '@angular/core';
import { Migrant } from '@/_models';
import { MigrantService } from '@/_services';
import { NgForm } from '@angular/forms';

declare var M: any;
@Component({
  selector: 'Migrant',
  templateUrl: './migrants.components.html',
  providers: [MigrantService]
})
export class MigrantComponent implements OnInit {
  constructor(private migrantService: MigrantService) {}
  ngOnInit() {
    this.getMigrants();
  }
  addMigrant(form?: NgForm) {
    console.log(form.value);
    if (form.value._id) {
      this.migrantService.putMigrant(form.value).subscribe(res => {
        this.resetForm(form);
        this.getMigrants();
        M.toast({ html: 'Migrante Actualizado' });
      });
    } else {
      this.migrantService.postMigrant(form.value).subscribe(res => {
        this.getMigrants();
        this.resetForm(form);
        M.toast({ html: 'Migrante Archivado' });
      });
    }
  }
  getMigrants() {
    this.migrantService.getMigrant().subscribe(res => {
      this.migrantService.Migrant = res as Migrant[];
    });
  }
  editMigrant(migrant: Migrant) {
    this.migrantService.selectedMigrant = migrant;
  }
  deleteMigrant(_id: string, form: NgForm) {
    if (confirm('Esta seguro que quiere borrar estos datos?')) {
      this.migrantService.deleteMigrant(_id).subscribe(res => {
        this.getMigrants();
        M.toast({ html: 'Datos borrados con éxito' });
      });
    }
  }

  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.migrantService.selectedMigrant = new Migrant();
    }
  }
}
