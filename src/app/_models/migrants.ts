export class Migrant {
  _id: number;

  person: {
    names: String;
    lastNames: String;
    age: Number;
    gender: String;
    maritalStatus: [];
    childrensNumbers: Number;
    childrensAges: [];
    childrensCountry: String;
    language: String;
    othersLanguage: [];
    driverLicence: Boolean;
    driverLicenceType: String;
  };
  migration: {
    countryOfBirth: String;
    timeInUy: Number;
    traveledBy: String;
    cameWith: String;
    uyTransitStatus: Boolean;
    traveledWithChildrens: Boolean;
    cameFromAnotherCountry: Boolean;
    howLongAnotherCountry: Number;
    whyChooseUY: String;
    kindOfMigrationStat: String;
    documentKind: String;
    documentID: Number;
    wealthID: Boolean;
    previdenceID: Boolean;
  };
  health: {
    healthSystem: Boolean;
    healthSystemName: String;
    accesibility: Boolean;
    accesibilityName: String;
    illness: String;
    medicalSupply: String;
    medicalReference: String;
  };
  work: {
    actuallyWork: Boolean;
    profession: String;
    boxRegistered: String;
    timeOnQueue: Number;
    timeStillOnQueue: Number;
    curriculumMade: Boolean;
    workAssistance: Boolean;
    curriculumByVolunteer: Boolean;
    webSitesRegistered: [];
  };
  contact: {
    phone: Boolean;
    phoneNumber: String;
    hasEmail: Boolean;
    email: String;
    hasSocialMedia: Boolean;
    socialMedia: [];
  };
  relationship: {
    mediaTargeted: [];
    mediaVisit: String;
    reasonToVisit: String;
    recomendedByOther: String;
    otherAssistance: String;
  };
  education: {
    educationLevel: String;
    profession: String;
    studyOnUY: Boolean;
    wantStudyOnUy: Boolean;
    documentsStudy: Boolean;
    educationAid: Boolean;
    formerCapacity: Boolean;
    educationCapacityArea: [];
  };
  living: {
    ownLiving: String;
    kindOfLiving: String;
    address: String;
    basicServicesList: [];
  };
}
