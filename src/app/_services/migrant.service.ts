import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Migrant } from '@/_models';

@Injectable({
  providedIn: 'root'
})
export class MigrantService {
  selectedMigrant: Migrant;
  Migrant: Migrant[];

  readonly URL_API = `http://localhost:3000/api/migrant`;

  constructor(private http: HttpClient) {
    this.selectedMigrant = new Migrant();
  }
  postMigrant(migrant: Migrant) {
    return this.http.post(this.URL_API, migrant);
  }
  getMigrant() {
    return this.http.get(this.URL_API);
  }
  putMigrant(migrant: Migrant) {
    return this.http.put(this.URL_API + `/${migrant._id}`, migrant);
  }
  deleteMigrant(_id: string) {
    return this.http.delete(this.URL_API + `/${_id}`);
  }
}
